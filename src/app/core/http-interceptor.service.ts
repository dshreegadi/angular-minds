import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const API_KEY = '71f8749f1de88882b0a2ea8cb3e77863';
    req = req.clone({
      setHeaders: {
        'user-key': API_KEY
      }
    });
    return next.handle(req);

  }
}