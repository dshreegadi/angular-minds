import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyCollectionRoutingModule } from './my-collection-routing.module';
import { CollectionComponent } from './collection/collection.component';


@NgModule({
  declarations: [CollectionComponent],
  imports: [
    CommonModule,
    MyCollectionRoutingModule
  ]
})
export class MyCollectionModule { }
