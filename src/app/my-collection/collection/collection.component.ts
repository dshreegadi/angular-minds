import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {

  constructor(
    private _dataService: DataService
  ) { }

  collections = [];

  ngOnInit(): void {
    this.getCollections();
  }


  getCollections() {
    this._dataService.getCollections().subscribe(
      (res) => {
        this.collections = res['collections'];
      }, (err) => { }
    );
  }

}
