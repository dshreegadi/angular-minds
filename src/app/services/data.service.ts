import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor( private http: HttpClient) { }

  getCollections() : Observable<any> {
    const API_URL = `https://developers.zomato.com/api/v2.1/collections?city_id=280`;
    return this.http.get(API_URL);
  }
}
